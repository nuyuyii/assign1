from django.db import models


class Item(models.Model):   
    goal = models.TextField(default='')
    money = models.FloatField(default=0)
    time = models.IntegerField(default=0)
    rate = models.FloatField(default=0)
