# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SaveMoney', '0006_auto_20150512_0036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='time',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
