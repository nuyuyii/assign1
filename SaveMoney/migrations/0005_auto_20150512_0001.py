# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SaveMoney', '0004_auto_20150512_0000'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='ear',
            new_name='year',
        ),
    ]
