# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SaveMoney', '0002_item_text'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='text',
            new_name='goal',
        ),
        migrations.AddField(
            model_name='item',
            name='money',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='rate',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='year',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
    ]
