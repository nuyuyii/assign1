# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SaveMoney', '0005_auto_20150512_0001'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='year',
            new_name='time',
        ),
    ]
