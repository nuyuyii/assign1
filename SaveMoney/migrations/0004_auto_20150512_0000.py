# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SaveMoney', '0003_auto_20150402_0837'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='year',
            new_name='ear',
        ),
    ]
