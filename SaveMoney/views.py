from django.shortcuts import redirect, render
from django.db import reset_queries
from SaveMoney.models import Item
import operator

def home_page(request):
    if(request.method == 'POST'and request.POST.get(
                                   'button_cal','') == 'calculate'):
        Item.objects.all().delete() #delete database
        goal_text = request.POST['goal']
        money_text = float(request.POST['money'])
        time_text = int(request.POST['time'])
        rate_text = float(request.POST['rate'])
        Item.objects.create(
                goal = goal_text,
                money = money_text,
                time = time_text,
                rate = rate_text
               )
        reset_queries()
        return redirect('/calculate/')
    items = Item.objects.all()

    return render(request, 'home.html')

# calculate
def result_page(request):
    Rate_total = 0
    Rate = 0
    Money = 0
    data_time = 0
    data_money = 0
    data_rate = 0
    all_money = []
    all_rate = []
    all_time = []
    items = Item.objects.all()
    for i in items:
        Money = i.money
        for num in range(0,i.time):
            Rate = float(i.rate)*float(Money)/100
            Money = float(Money) + float(Rate)
            Rate_total = float(Rate_total)+float(Rate)
            all_money.append(Money)
            all_rate.append(Rate_total)
            all_time.append(num)

    if(request.method == 'POST' and request.POST.get(
       'Home_send', '') == 'send'):
        return redirect('/')

    return render(request, 'calculate.html',
                  {'items':items,'total_money':Money,
                   'total_rate':Rate_total, 'all_times':all_time,
                   'all_moneys':all_money,'all_rates':all_rate})


def TotalMoney(year, money, rate):
    Rate_All = 0
    for num in range(0,year):
        Rate = float(rate)*float(money)/100
        money = float(money) + float(Rate)
        Rate_All = float(Rate_All)+float(Rate)
    return (money,Rate_All)
    #all_money, all_rate = TotalMoney(items.year,items.money,items.rate)
