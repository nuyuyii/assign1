from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from SaveMoney.models import Item 
import time

class NewVisitorTest(LiveServerTestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Saving', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('money', header_text)

        # She is invited to enter a to-do item straight away
        # test1 input box

        inputbox_goal = self.browser.find_element_by_id('id_goal')
        self.assertEqual(
                inputbox_goal.get_attribute('placeholder'),
                'Enter your goal'
        )
        inputbox_money = self.browser.find_element_by_id('id_money')
        self.assertEqual(
                inputbox_money.get_attribute('placeholder'),
                'Money'
        )
        inputbox_time = self.browser.find_element_by_id('id_time')
        self.assertEqual(
                inputbox_time.get_attribute('placeholder'),
                'Time'
        )
        inputbox_rate = self.browser.find_element_by_id('id_rate')
        self.assertEqual(
                inputbox_rate.get_attribute('placeholder'),
                'Rate'
        )
        inputbox_goal.send_keys('เก็บเงินวันละนิดเพื่ออนาคตกันนะคะ')
        inputbox_money.send_keys(200)
        inputbox_time.send_keys(3)
        inputbox_rate.send_keys(20)
        button = self.browser.find_element_by_id('button')
        button.click()
        time.sleep(5)
        self.check_for_row_in_list_table('เป้าหมายการออม: เก็บเงินวันละนิดเพื่ออนาคตกันนะคะ')
        self.check_for_row_in_list_table('เงินออมทั้งหมด: 345.6')
        self.check_for_row_in_list_table('ดอกเบี้ย: 145.6')
        Home = self.browser.find_element_by_id('button')
        Home.click()
        time.sleep(5)
        self.fail('Finish the test!')


