from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       url(r'^$', 'SaveMoney.views.home_page', name='home'),
                       url(r'calculate/$',
                           'SaveMoney.views.result_page',
                           name='calculate'),
                      )
